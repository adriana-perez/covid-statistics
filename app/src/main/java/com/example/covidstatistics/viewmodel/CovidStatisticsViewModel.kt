package com.example.covidstatistics.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.coviddatamodule.api.CovidApiService
import com.example.covidstatistics.di.common.ActivityScope
import com.example.coviddatamodule.model.CovidStatistics
import com.example.coviddatamodule.model.CovidStatisticsDetails
import com.example.covidstatistics.R
import com.example.covidstatistics.UIResponseState
import java.util.*
import javax.inject.Inject

@ActivityScope
class CovidStatisticsViewModel @Inject constructor(val api: CovidApiService): ViewModel() {

    private val covid = "CovidStatisticsViewModel"
    private var maxDate: Long? = null

    private val _viewState = MutableLiveData<UIResponseState>().apply {
        value = UIResponseState.Loading
    }

    val viewState: LiveData<UIResponseState> = _viewState

    var statistics = MutableLiveData<CovidStatistics>().apply {
        value = CovidStatistics(
            data = CovidStatisticsDetails(
                date = "", confirmed = 0, deaths = 0
            )
        )
    }

    private val messageLabel = MutableLiveData<Int>().apply {
        value = R.string.check_connection
    }

    private val checkResponseLabel = MutableLiveData<Int>().apply {
        value = R.string.check_response
    }

    fun setLoading() {
        _viewState.postValue(UIResponseState.Loading)
    }

    suspend fun getStatics(date: String) {
        try {
            val response = api.getData(date)

            Log.d(covid, "body -> " + response)

            response.body()?.let {
                _viewState.postValue(UIResponseState.Success(it))
                statistics.postValue(it)
            } ?: run {
                _viewState.postValue(UIResponseState.Error(messageLabel.toString()))
            }
        } catch (e: Exception) {
            Log.d(covid, "error -> $e")
            _viewState.postValue(UIResponseState.Error(checkResponseLabel.toString()))
        }

    }

    fun getMaxDate(): Long? {
        if (maxDate == null) {
            var prevDay: Calendar = Calendar.getInstance().clone() as Calendar
            prevDay.add(Calendar.DAY_OF_MONTH, -1)
            maxDate = prevDay.timeInMillis
        }
        return maxDate
    }
 }
