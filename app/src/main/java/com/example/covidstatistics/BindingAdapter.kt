package com.example.covidstatistics

import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


@BindingAdapter("hideOnLoading")
fun ViewGroup.hideOnLoading(responseState: UIResponseState) {
    visibility = if (responseState is UIResponseState.Loading)
        View.GONE
    else
        View.VISIBLE
}

@BindingAdapter("showOnLoading")
fun ProgressBar.showOnLoading(responseState: UIResponseState) {
    visibility = if (responseState is UIResponseState.Loading)
        View.VISIBLE
    else
        View.GONE
}

@BindingAdapter("showOnError")
fun TextView.showOnError(responseState: UIResponseState) {
    visibility = if (responseState is UIResponseState.Error) {
        Toast.makeText(rootView.context, (responseState).errorMessage, Toast.LENGTH_SHORT).show();
        View.VISIBLE
    } else
        View.GONE
}

@BindingAdapter("bindDate")
fun TextView.bindDate(date: String?) {
    if(!date.isNullOrEmpty()){
        val spanishLocale = Locale("es", "ES")
        val inputFormatter = SimpleDateFormat("yyyy-MM-dd")
        val date = inputFormatter.parse(date)

        val outputFormatter = DateFormat.getDateInstance(DateFormat.LONG, spanishLocale)
        val formatted = outputFormatter.format(date)

        text = formatted
    }
}