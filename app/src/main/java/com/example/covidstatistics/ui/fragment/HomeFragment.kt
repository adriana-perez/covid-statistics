package com.example.covidstatistics.ui.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.coviddatamodule.api.CovidApiService
import com.example.coviddatamodule.repository.CovidRepository
import com.example.covidstatistics.R
import com.example.covidstatistics.viewmodel.CovidStatisticsViewModel
import com.example.covidstatistics.databinding.FragmentHomeBinding
import com.example.covidstatistics.di.application.ApplicationCovid
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton




class HomeFragment: Fragment() , DatePickerDialog.OnDateSetListener {
//    private val router = RouterApplication()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: CovidStatisticsViewModel by viewModels { viewModelFactory }

    private val coroutineScope = CoroutineScope(Dispatchers.Main)
    private lateinit var binding: FragmentHomeBinding
    private lateinit var datePickerDialog: DatePickerDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        router.onCreate()
        setInjection()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        binding.viewModel = viewModel

        datePickerDialog = DatePickerDialog(requireContext(),
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)-1)

        binding.dataPicker = datePickerDialog

        datePickerDialog.datePicker.maxDate = viewModel.getMaxDate()!!

        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1)
        val currentDate = sdf.format(calendar.time)

        binding.lifecycleOwner = this

        coroutineScope.launch {
            viewModel.getStatics(currentDate)
        }

        return binding.root
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        coroutineScope.launch {
            val month = month + 1
            val fixedMonth = if (month >= 10) month else "0$month"
            val fixedDay = if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth"
            viewModel.setLoading()
            viewModel.getStatics("$year-$fixedMonth-$fixedDay")
        }
    }

    private fun setInjection() {
        (context?.applicationContext as ApplicationCovid).getComponent().inject(this)
    }
}