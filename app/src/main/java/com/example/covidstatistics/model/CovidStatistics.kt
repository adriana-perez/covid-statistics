package com.example.covidstatistics.model

import com.google.gson.annotations.SerializedName

data class CovidStatistics (
    @SerializedName("data") var data: CovidStatisticsDetails
)
