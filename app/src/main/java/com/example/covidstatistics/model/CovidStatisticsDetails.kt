package com.example.covidstatistics.model

import com.google.gson.annotations.SerializedName

data class CovidStatisticsDetails (
    @SerializedName("date") var date:String,
    @SerializedName("confirmed") var confirmed:Int,
    @SerializedName("deaths") var deaths:Int
)
