package com.example.covidstatistics.di.application

import android.app.Application
import com.example.covidstatistics.di.AppComponent
import com.example.covidstatistics.di.DaggerAppComponent

class ApplicationCovid: Application() {
    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().build()
    }

    fun getComponent() = appComponent
}