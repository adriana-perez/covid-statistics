package com.example.covidstatistics.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.covidstatistics.di.common.ViewModelFactory
import com.example.covidstatistics.di.common.ViewModelKey
import com.example.covidstatistics.viewmodel.CovidStatisticsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class PresentationModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CovidStatisticsViewModel::class)
    abstract fun bindCovidInfoViewModel(viewModel: CovidStatisticsViewModel): ViewModel
}
