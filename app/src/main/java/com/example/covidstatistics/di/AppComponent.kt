package com.example.covidstatistics.di

import com.example.covidstatistics.di.common.ActivityScope
import com.example.covidstatistics.ui.fragment.HomeFragment
import com.example.routerservicemodule.di.RouterCovidDataModule
import dagger.Component
import javax.inject.Singleton

@ActivityScope
@Component(modules = [
    PresentationModule::class,
    RouterCovidDataModule::class
]
)
@Singleton
interface AppComponent{
    fun inject(fragment: HomeFragment)
}